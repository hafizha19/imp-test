<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        try {
            $users = User::paginate(2);
            return responseHelper(true, 200, $users);
        } catch (\Throwable $th) {
            return responseHelper(false, 500, '', '', $th->getMessage());
        }
    }
}
