@extends('layouts.app')
@section('content')
    <div class="row justify-content-between">
        <div class="col-lg-3">
            <h2>All of candidates</h2>
        </div>
        <div class="col-lg-2">
            <a class="btn btn-success btn-block" href="{{ route('candidate.create') }}">Add Candidate</a>
        </div>
    </div>
    @if (\Session::has('success'))
        <div class="alert alert-success mb-1 mt-1">
            {!! \Session::get('success') !!}
        </div>
    @elseif (\Session::has('failed'))
        <div class="alert alert-danger mb-1 mt-1">
            {!! \Session::get('failed') !!}
        </div>
    @endif
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center">S.No</th>
                <th class="text-center">Name</th>
                <th class="text-center">Gender</th>
                <th class="text-center">Date of Birth</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $d)
                <tr>
                    <td>{{ $d['id'] }}</td>
                    <td>{{ $d['full_name'] }}</td>
                    <td>{{ $d['gender'] }}</td>
                    <td>{{ $d['dob'] }}</td>
                    <td>
                        <div class="row justify-content-center">
                            <a class="btn btn-primary" href="{{ route('candidate.edit', $d['id']) }}">Edit</a>
                            <form action="{{ route('candidate.destroy', $d['id']) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
