<?php

use App\Http\Controllers\CandidateController;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('candidates', [CandidateController::class, 'index'])->name('candidate.index');
Route::get('candidate/create', [CandidateController::class, 'create'])->name('candidate.create');
Route::post('candidate/store', [CandidateController::class, 'store'])->name('candidate.store');
Route::get('candidate/{id}', [CandidateController::class, 'edit'])->name('candidate.edit');
Route::post('candidate/{id}', [CandidateController::class, 'update'])->name('candidate.update');
Route::delete('candidate/{id}', [CandidateController::class, 'destroy'])->name('candidate.destroy');