@extends('layouts.app')
@section('content')
    <h3 class="text-center">Step 0</h3>
    <div class="row justify-content-between p-2">
        <div class="col-lg-3">
            <h6>Personal Details</h6>
        </div>
        <div class="col-lg-2">
            <a class="btn btn-success btn-block" href="{{ route('candidate.index') }}">Back</a>
        </div>
    </div>
    @if (!empty($success))
        <div class="alert alert-success mb-1 mt-1">
            {{ $success }}
        </div>
    @elseif (!empty($failed))
        <div class="alert alert-danger mb-1 mt-1">
            {{ $failed }}
        </div>
    @endif
    <div class="p-2">
        <form action="{{ route('candidate.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="full_name">Full Name</label>
                        <input type="text" class="form-control" name="full_name" aria-describedby="emailHelp"
                            placeholder="Your name" required>
                    </div>
                    <div class="form-group">
                        <label for="dob">Date of Birth</label>
                        <input type="date" class="form-control" name="dob" required>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <select class="form-control" name="gender" required>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </form>
    </div>
@endsection
