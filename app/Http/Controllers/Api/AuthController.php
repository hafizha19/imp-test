<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'username'  => 'required|min:2|unique:users',
                'password'  => 'required|min:5'
            ]);

            if ($validator->fails()) {
                return responseHelper(false, 422, '', '', $validator->errors());
            }

            $user = User::where('username', $request->username)->first();

            if (empty($user)) {
                $user = User::create([
                    'name'      => $request->name,
                    'username'  => $request->username,
                    'password'  => bcrypt($request->password)
                ]);
            } else {
                return responseHelper(false, 409);
            }

            $credentials = $request->only('username', 'password');
            $token = auth()->guard('api')->attempt($credentials);

            if ($user) {
                $data = [
                    'user' => $user,
                    'authorization' => [
                        'token' => $token,
                        'type' => 'bearer',
                    ]
                ];
                return responseHelper(true, 200, $data, 'User created successfully');
            }
        } catch (\Throwable $th) {
            return responseHelper(false, 500, '', '', $th->getMessage());
        }
    }

    public function login(Request $request)
    {
        try {
            $request->validate([
                'username'  => 'required|min:2',
                'password'  => 'required|min:5'
            ]);

            $credentials = $request->only('username', 'password');
            $token = auth()->guard('api')->attempt($credentials);

            if (!$token) {
                return responseHelper(false, 401);
            }

            $data = [
                'user' => auth()->guard('api')->user(),
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ];
            return responseHelper(true, 200, $data);
        } catch (\Throwable $th) {
            return responseHelper(false, 500, '', '', $th->getMessage());
        }
    }
}
