<?php

if (!function_exists('responseHelper')) {
    function responseHelper($success, $code, $data = '', $customMessage = '', $errors = '')
    {
        switch ($code) {
            case '200':
                $message = 'Success';
                break;
            case '404':
                $message = 'Not found';
                break;
            case '401':
                $message = 'Not Authorized';
                break;
            case '400':
                $message = 'Bad Request';
                break;
            case '409':
                $message = 'User already exists';
                break;
            case '422':
                $message = 'Error validations';
                break;
            default:
                $message = 'Internal server error';
                break;
        }

        $response = [
            'success' => $success,
            'message' => !empty($customMessage) ? $customMessage : $message,
            'data'    => $data
        ];
        if (!$success) {
            $response['errors'] = $errors;
        }

        return response()->json($response, $code);
    }
}
