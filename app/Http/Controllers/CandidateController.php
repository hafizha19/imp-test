<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CandidateController extends Controller
{
    public function index()
    {
        $data = Http::get('http://127.0.0.1:8001/api/candidates')['data'];
        return view('index', compact('data'));
    }

    public function create()
    {
        return view('create');
    }

    public function edit($id)
    {
        $data = Http::get('http://127.0.0.1:8001/api/candidate/'.$id)['data'];
        return view('edit', compact('data'));
    }

    public function store(Request $request)
    {
        $data = [
            'full_name' => $request->full_name,
            'dob' => $request->dob,
            'gender' => $request->gender
        ];
        $response = Http::withBody(json_encode($data), 'application/json')
            ->post('http://127.0.0.1:8001/api/candidate/create');
        if ($response['success']) {
            return view('create')->with('success','Candidate created successfully');;
        } else {
            return view('create')->with('failed','Candidate failed to create');;
        }
    }

    public function update(Request $request, $id)
    {
        $data = [
            'full_name' => $request->full_name,
            'dob' => $request->dob,
            'gender' => $request->gender
        ];
        $response = Http::withBody(json_encode($data), 'application/json')
            ->put('http://127.0.0.1:8001/api/candidate/'.$id);
        if ($response['success']) {
            return view('edit', ['data' => $response['data']])->with('success','Candidate updated successfully');;
        } else {
            return view('edit', compact('data'))->with('failed','Candidate failed to update');
        }
    }

    public function destroy($id)
    {
        $response = Http::delete('http://127.0.0.1:8001/api/candidate/'.$id);
        if ($response['success']) {
            return redirect()->back()->with(['success' => 'Candidate deleted successfully']);
        } else {
            return redirect()->back()->with(['failed' => 'Candidate failed to deleted']);
        }
    }
}
